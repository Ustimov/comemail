public interface NewLetterListener {
    public void addLetter(Letter letter);

    public void refresh();
}
