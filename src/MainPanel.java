import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {
    private App parent;

    private JTabbedPane tabbedPane;

    private LetterPanel inLetterPanel;
    private LetterPanel outLetterPanel;

    public MainPanel(App parent) {
        super(new BorderLayout());

        this.parent = parent;

        inLetterPanel = new LetterPanel(this.parent, true);
        outLetterPanel = new LetterPanel(this.parent, false);

        createUiComponents();
    }

    private void createUiComponents() {
        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Входящие", inLetterPanel);
        tabbedPane.addTab("Отправленные", outLetterPanel);
        this.add(tabbedPane, BorderLayout.CENTER);
    }

    public LetterPanel getInLetterPanel() {
        return inLetterPanel;
    }

    public LetterPanel getOutLetterPanel() {
        return outLetterPanel;
    }
}
