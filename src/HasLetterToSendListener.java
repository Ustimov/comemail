public interface HasLetterToSendListener {
    public void sendLetter(byte[] bytes);
}
