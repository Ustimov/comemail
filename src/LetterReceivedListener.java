public interface LetterReceivedListener {
    public void recvLetter(byte[] bytes);
}
