import javax.swing.*;
import java.awt.*;

public class ReadLetterPanel extends JPanel {
    private App parent;

    private JLabel sender;
    private JLabel title;
    private JLabel text;
    private JLabel senderAddress;

    private Letter letter;

    private boolean isIncoming;

    public ReadLetterPanel(App parent, boolean isIncoming) {
        super(new GridBagLayout());

        this.parent = parent;
        this.isIncoming = isIncoming;

        createUiComponents();
    }

    private void createUiComponents() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;

        JLabel titleLabel = new JLabel("Заголовок:");
        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD));
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        this.add(titleLabel, gridBagConstraints);

        title = new JLabel();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 2;
        gridBagConstraints.gridwidth = 2;
        this.add(title, gridBagConstraints);


        JLabel senderName = new JLabel();

        if (isIncoming) {
            senderName.setText("Имя отправителя:");
        } else {
            senderName.setText("Имя получателя:");
        }

        senderName.setFont(senderName.getFont().deriveFont(Font.BOLD));
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 1;
        this.add(senderName, gridBagConstraints);

        sender = new JLabel();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 2;
        this.add(sender, gridBagConstraints);

        JLabel senderAddressLabel = new JLabel();

        if (isIncoming) {
            senderAddressLabel.setText("Адрес отправителя:");
        } else {
            senderAddressLabel.setText("Адрес получателя:");
        }

        senderAddressLabel.setFont(senderAddressLabel.getFont().deriveFont(Font.BOLD));
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 1;
        this.add(senderAddressLabel, gridBagConstraints);

        senderAddress = new JLabel();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 2;
        this.add(senderAddress, gridBagConstraints);

        JLabel textLabel = new JLabel("Текст сообщения:");
        textLabel.setFont(textLabel.getFont().deriveFont(Font.BOLD));
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.weightx = 3;
        gridBagConstraints.gridwidth = 3;
        this.add(textLabel, gridBagConstraints);

        text = new JLabel();
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1;
        this.add(text, gridBagConstraints);

        JButton homeButton = new JButton("На главную");
        homeButton.setName("RHome");
        homeButton.addActionListener(this.parent);
        gridBagConstraints.gridy = 5;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.weighty = 0;
        this.add(homeButton, gridBagConstraints);

        if (isIncoming) {
            JButton answerButton = new JButton("Ответить");
            answerButton.setName("Answer");
            answerButton.addActionListener(this.parent);
            gridBagConstraints.gridx = 2;
            this.add(answerButton, gridBagConstraints);
        }
    }

    public Letter getLetter() {
        return letter;
    }

    public void setLetter(Letter letter) {
        if (isIncoming) {
            sender.setText(letter.sender.getName());
            senderAddress.setText(letter.sender.address);
        } else {
            sender.setText(letter.receiver.getName());
            senderAddress.setText(letter.receiver.address);
        }

        title.setText(letter.title);
        text.setText(letter.message);

        this.letter = letter;
    }
}
