import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.util.Enumeration;

public class LetterPanel extends JPanel implements NewLetterListener {
    private App parent;

    private JList letterList;

    private DefaultListModel listModel;

    boolean isIncoming;

    public LetterPanel(App parent, boolean isIncoming) {
        super(new BorderLayout());

        this.parent = parent;
        this.isIncoming = isIncoming;

        createUiComponents();
    }

    private void createUiComponents() {
        listModel = new DefaultListModel();
        letterList = new JList(listModel);

        if (isIncoming) {
            letterList.setName("Incoming");
        } else {
            letterList.setName("Other");
        }

        letterList.addListSelectionListener(this.parent);
        letterList.getFont().deriveFont(Font.PLAIN);

        Border lineBorder = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY);
        Border margin = BorderFactory.createEmptyBorder(5, 5, 5, 5);

        this.setBorder(new CompoundBorder(margin, lineBorder));
        this.add(letterList, BorderLayout.CENTER);
    }

    public void addLetter(Letter letter) {
        if (letter.getStatus() == LetterStatus.SENT && !isIncoming) {
            listModel.insertElementAt(letter, 0);
            this.updateUI();
        } else if (letter.status == LetterStatus.RECIEVED && isIncoming) {
            listModel.insertElementAt(letter, 0);
            this.updateUI();
        }
    }

    public void refresh() {
        this.updateUI();
    }
}
