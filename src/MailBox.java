import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.Timer;

public class MailBox implements LetterReceivedListener {
    private ArrayList<HasLetterToSendListener> hasLetterListeners = new ArrayList<>();
    private ArrayList<NewLetterListener> newLetterListeners = new ArrayList<>();
    private ArrayList<Letter> letters = new ArrayList<>();

    private LinkedList<Letter> sendQueue = new LinkedList<>();

    private User user;
    User redirectUser = new User();

    boolean isRedirectionEnabled;

    private Date tokenDate;

    private Timer timer;

    private App app;

    public MailBox(User user, App app) {
        this.user = user;
        this.app = app;
    }

    public void updateLists() {
        for (Letter letter : letters) {
            for (NewLetterListener listener : newLetterListeners) {
                listener.addLetter(letter);
            }
        }
    }

    public void refreshLists() {
        for (Letter letter : letters) {
            for (NewLetterListener listener : newLetterListeners) {
                listener.refresh();
            }
        }
    }

    public void clearNewLetterListeners() {
        newLetterListeners.clear();
    }

    public void recvLetter(byte[] bytes) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            Object obj = objectInputStream.readObject();

            if (obj instanceof Token) {
                System.out.println(user.address + " receive the token");

                if (timer != null) {
                    timer.stop();
                }

                timer = new Timer(5000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        app.onTokenLost();
                    }
                });

                timer.start();

                sendLetter();
            } else if (obj instanceof Letter) {
                Letter letter = (Letter) obj;

                if (letter.receiver.address.equals(user.address)) {
                    // If letter come to receiver.
                    if (isRedirectionEnabled) {
                        letter.receiver.address = redirectUser.address;
                        sendQueue.addLast(letter);
                        System.out.println(user.address + ": letter has been redirected to " + letter.receiver.address);
                        return;
                    }
                    System.out.println(user.address + ": letter to me has been delivered");
                    letter.setStatus(LetterStatus.RECIEVED);
                    letter.receiver = user;
                    addLetter(letter);
                    if (!letter.sender.address.equals(user.address)) {
                        addToSendQueue(letter);
                    }
                } else if (letter.sender.address.equals(user.address)) {
                    // If letter come to sender (as confirmation of delivery or opening).
                    if (letter.getStatus() == LetterStatus.RECIEVED) {
                        if (letter.isOpened) {
                            markLetterAsOpened(letter.getLetterHashCode());
                            refreshLists();
                        } else {
                            letter.setStatus(LetterStatus.SENT);
                            addLetter(letter);
                        }
                        System.out.println(user.address + ": letter to " + letter.receiver.address + "has been delivered");
                    } else {
                        // If letter is redirected to PC that goes early than redirecting one.
                        if (letter.counter == 0) {
                            letter.counter++;
                            sendQueue.addFirst(letter);
                        }
                    }
                } else {
                    sendQueue.addFirst(letter);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Receive letter exception");
        }
    }

    public void addSendListener(HasLetterToSendListener listener) {
        hasLetterListeners.add(listener);
    }

    public void addToSendQueue(Letter letter) {
        sendQueue.add(letter);
    }

    private void addLetter(Letter letter) {
        letters.add(letter);

        for (NewLetterListener newLetterListener : newLetterListeners) {
            newLetterListener.addLetter(letter);
        }
    }

    public User getUser() {
        return user;
    }

    private void sendLetter() throws Exception {
        if (!sendQueue.isEmpty()) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

            Letter letter = sendQueue.removeFirst();
            System.out.println(user.firstName + " send letter to " + letter.receiver.address);

            if (letter.status == LetterStatus.WAITING) {
                letter.status = LetterStatus.SENT;
            }

            objectOutputStream.writeObject(letter);

            for (HasLetterToSendListener listener : hasLetterListeners) {
                listener.sendLetter(byteArrayOutputStream.toByteArray());
            }
        }

        Thread.sleep(1000);

        sendToken();
    }

    private void markLetterAsOpened(int hashCode) {
        for (Letter letter : letters) {
            if (letter.message.hashCode() == hashCode && !letter.isOpened) {
                letter.isOpened = true;
                break;
            }
        }
    }

    public void sendToken() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(new Token());

            for (HasLetterToSendListener listener : hasLetterListeners) {
                listener.sendLetter(byteArrayOutputStream.toByteArray());
            }
        } catch (Exception e) {
            System.out.println("Send token exception");
        }
    }

    public void loadLetters() {
        try {
            Path file = Paths.get(user.address);
            byte[] bytes = Files.readAllBytes(file);

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            letters = (ArrayList<Letter>) objectInputStream.readObject();
        } catch (Exception e) {
            System.out.println("Load exception");
        }
    }

    public void storeLetters() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(letters);

            Path file = Paths.get(user.address);
            Files.write(file, byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            System.out.println("Store exception");
        }
    }

    public void addNewLetterListener(NewLetterListener listener) {
        newLetterListeners.add(listener);
    }
}
