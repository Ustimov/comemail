import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class App extends JFrame implements ActionListener, ListSelectionListener {
    private ConnectPanel connectPanel;// = new ConnectPanel(this);
    private MainPanel mainPanel;// = new MainPanel(this);
    private WriteLetterPanel writeLetterPanel;// = new WriteLetterPanel(this);
    private ReadLetterPanel readLetterPanel;// = new ReadLetterPanel(this);

    private MailBox mailBox;
    private Postman postman;

    JMenu file;
    JMenu letter;
    JMenu connection;
    JMenu redirection;

    public App() {
        createUiComponets();
    }

    private void createUiComponets() {
        setTitle("COM E-mail");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        createMenuBar();

        connectPanel = new ConnectPanel(this);
        this.add(connectPanel);
        this.pack();
    }

    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        file = new JMenu("Файл");
        letter = new JMenu("Письма");
        connection = new JMenu("Соединение");
        redirection = new JMenu("Переадресация");

        JMenuItem exitMenuItem = new JMenuItem("Выход");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        JMenuItem writeMenuItem = new JMenuItem("Написать письмо");
        writeMenuItem.setName("Write");
        writeMenuItem.addActionListener(this);

        JMenuItem sendTokenMenuItem = new JMenuItem("Отправить маркер");
        sendTokenMenuItem.setName("Token");
        sendTokenMenuItem.addActionListener(this);

        JMenuItem setUpRedirection = new JMenuItem("Настроить переадресацию");
        final App app = this;
        setUpRedirection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String redirectAddress = JOptionPane.showInputDialog(app, "Введите адрес для переадресации",
                        "Настройка переадресации", JOptionPane.QUESTION_MESSAGE);
                mailBox.redirectUser.address = redirectAddress;
            }
        });

        JCheckBoxMenuItem enableRedirection = new JCheckBoxMenuItem("Включить переадресацию");
        enableRedirection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (mailBox.isRedirectionEnabled) {
                    mailBox.isRedirectionEnabled = false;
                } else {
                    mailBox.isRedirectionEnabled = true;
                }
            }
        });

        JMenuItem storeLetters = new JMenuItem("Сохранить письма");
        storeLetters.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mailBox.storeLetters();
            }
        });

        JMenuItem loadLetters = new JMenuItem("Загрузить письма");
        loadLetters.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mailBox.loadLetters();
                mailBox.updateLists();
            }
        });

        file.add(exitMenuItem);
        letter.add(writeMenuItem);
        letter.add(storeLetters);
        letter.add(loadLetters);
        connection.add(sendTokenMenuItem);
        redirection.add(setUpRedirection);
        redirection.add(enableRedirection);

        menuBar.add(file);
        menuBar.add(letter);
        menuBar.add(connection);
        menuBar.add(redirection);

        letter.setEnabled(false);
        connection.setEnabled(false);
        redirection.setEnabled(false);

        setJMenuBar(menuBar);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.print("Set look and feel exception");
        }
        App app = new App();
        app.setVisible(true);
    }

    private void onConnectButtonClick() {
        if (!connectPanel.isReady()) {
            return;
        }

        mailBox = new MailBox(connectPanel.getUser(), this);
        postman = new Postman(connectPanel.getInPortName(), connectPanel.getOutPortName());

        mailBox.addSendListener(postman);
        postman.addRecvListener(mailBox);

        letter.setEnabled(true);
        connection.setEnabled(true);
        redirection.setEnabled(true);

        mainPanel = createMainPanel();

        this.remove(connectPanel);
        this.add(mainPanel);

        SwingUtilities.updateComponentTreeUI(this);
    }

    private MainPanel createMainPanel() {
        MainPanel mainPanel = new MainPanel(this);

        mailBox.clearNewLetterListeners();
        mailBox.addNewLetterListener(mainPanel.getInLetterPanel());
        mailBox.addNewLetterListener(mainPanel.getOutLetterPanel());
        mailBox.updateLists();

        return mainPanel;
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() instanceof JButton) {
            JButton button = (JButton) event.getSource();

            if (button.getName().equals("Connect")) {
                onConnectButtonClick();
            } else if (button.getName().equals("WHome")) {
                mainPanel = createMainPanel();

                this.remove(writeLetterPanel);
                this.add(mainPanel);

                SwingUtilities.updateComponentTreeUI(this);
            } else if (button.getName().equals("Send")) {
                onSendButtonClick();
            } else if (button.getName().equals("RHome")) {
                mainPanel = createMainPanel();

                this.remove(readLetterPanel);
                this.add(mainPanel);

                SwingUtilities.updateComponentTreeUI(this);
            } else if (button.getName().equals("Answer")) {
                onAnswerButtonClick();
            }
        } else if (event.getSource() instanceof JMenuItem) {
            JMenuItem menuItem = (JMenuItem) event.getSource();

            if (menuItem.getName().equals("Write")) {
                writeLetterPanel = new WriteLetterPanel(this);

                this.remove(mainPanel);
                this.add(writeLetterPanel);

                SwingUtilities.updateComponentTreeUI(this);
            } else if (menuItem.getName().equals("Token")) {
                mailBox.sendToken();
            }
        }
    }

    private void onSendButtonClick() {
        if (!writeLetterPanel.isReady()) {
            return;
        }

        Letter letter = writeLetterPanel.getLetter();

        if (!letter.receiver.address.equals(mailBox.getUser().address)) {
            letter.setSender(mailBox.getUser());
            mailBox.addToSendQueue(letter);
        } else {
            JOptionPane.showMessageDialog(this, "Нельзя отправлять письма себе",
                    "Ошибка", JOptionPane.INFORMATION_MESSAGE);
        }

        mainPanel = createMainPanel();

        this.remove(writeLetterPanel);
        this.add(mainPanel);

        SwingUtilities.updateComponentTreeUI(this);
    }

    private void onAnswerButtonClick() {
        Letter letter = readLetterPanel.getLetter();

        writeLetterPanel = new WriteLetterPanel(this);
        writeLetterPanel.setLetter(letter);

        this.remove(readLetterPanel);
        this.add(writeLetterPanel);

        SwingUtilities.updateComponentTreeUI(this);
    }

    public void valueChanged(ListSelectionEvent e) {
        JList list = (JList) e.getSource();

        if (list.getName().equals("Incoming")) {
            readLetterPanel = new ReadLetterPanel(this, true);
        } else {
            readLetterPanel = new ReadLetterPanel(this, false);
        }

        Letter letter = (Letter) list.getSelectedValue();
        readLetterPanel.setLetter(letter);

        if (!letter.isOpened && letter.getStatus() != LetterStatus.SENT) {
            letter.isOpened = true;
            mailBox.addToSendQueue(letter);
        }

        this.remove(mainPanel);
        this.add(readLetterPanel);

        SwingUtilities.updateComponentTreeUI(this);
    }

    public void onTokenLost() {
        JOptionPane.showMessageDialog(this, "Соединение потеряно",
                "Ошибка", JOptionPane.INFORMATION_MESSAGE);
    }
}
