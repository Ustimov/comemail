import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Letter implements Serializable {
    User sender;
    User receiver;

    String title;
    String message;

    LetterStatus status;

    Date date;

    int counter = 0;

    boolean isOpened = false;

    public Letter() {
        date = new Date();
    }

    public Letter(String title, String message, String address) {
        this.title = title;
        this.message = message;

        date = new Date();

        receiver = new User();
        receiver.address = address;

        status = LetterStatus.WAITING;
    }

    public String toString() {
        if(isOpened) {
            return new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(date) +
                    " " + "От: " + sender.getName() + " Кому: " + receiver.getName() + " " + title;
        } else {
            return "<html><b>" + new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(date) +
                    " " + "От: " + sender.getName() + " Кому: " + receiver.getName() + " " + title + "</b></html>";
        }
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public LetterStatus getStatus() {
        return status;
    }

    public void setStatus(LetterStatus status) {
        this.status = status;
    }

    public int getLetterHashCode() {
        return message.hashCode();
    }
}
