import javax.jws.soap.SOAPBinding;
import java.io.Serializable;

public class User implements Serializable {
    String firstName;
    String lastName;
    String address;

    public User() {

    }

    public User(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public String getName() {
        return firstName + " " + lastName;
    }
}
