import gnu.io.CommPortIdentifier;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ConnectPanel extends JPanel {
    private App parent;

    private JTextField firstNameTextField;
    private JTextField lastNameTextField;
    private JTextField addressTextField;
    private JComboBox outPortComboBox;
    private JComboBox inPortComboBox;

    public ConnectPanel(App app) {
        super(new GridBagLayout());
        this.parent = app;
        createUiComponents();
    }

    public User getUser() {
        return new User(firstNameTextField.getText(), lastNameTextField.getText(), addressTextField.getText());
    }

    public String getInPortName() {
        return inPortComboBox.getSelectedItem().toString();
    }

    public String getOutPortName() {
        return outPortComboBox.getSelectedItem().toString();
    }

    public boolean isReady() {
        // TODO: Make a validation of the fields.
        return true;
    }

    private void createUiComponents() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;

        JLabel firstNameLabel = new JLabel("Имя");
        gridBagConstraints.ipadx = 100;
        gridBagConstraints.insets = new Insets(5, 20, 5, 20);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        this.add(firstNameLabel, gridBagConstraints);

        firstNameTextField = new JTextField();
        gridBagConstraints.gridx = 1;
        this.add(firstNameTextField, gridBagConstraints);

        JLabel lastNameLabel = new JLabel("Фамилия");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        this.add(lastNameLabel, gridBagConstraints);

        lastNameTextField = new JTextField();
        gridBagConstraints.gridx = 1;
        this.add(lastNameTextField, gridBagConstraints);

        JLabel addressLabel = new JLabel("Адрес");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        this.add(addressLabel, gridBagConstraints);

        addressTextField = new JTextField();
        gridBagConstraints.gridx = 1;
        this.add(addressTextField, gridBagConstraints);

        JLabel inPortLabel = new JLabel("Порт 1");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        this.add(inPortLabel, gridBagConstraints);

        ArrayList<CommPortIdentifier> ports = Postman.getAvailableSerialPorts();
        DefaultComboBoxModel inComboBoxModel = new DefaultComboBoxModel();
        DefaultComboBoxModel outComboBoxModel = new DefaultComboBoxModel();
        for (CommPortIdentifier port : ports) {
            inComboBoxModel.addElement(port.getName());
            outComboBoxModel.addElement(port.getName());
        }

        inPortComboBox = new JComboBox();
        inPortComboBox.setModel(inComboBoxModel);
        gridBagConstraints.gridx = 1;
        this.add(inPortComboBox, gridBagConstraints);

        JLabel outPortLabel = new JLabel("Порт 2");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        this.add(outPortLabel, gridBagConstraints);

        outPortComboBox = new JComboBox();
        outPortComboBox.setModel(outComboBoxModel);
        gridBagConstraints.gridx = 1;
        this.add(outPortComboBox, gridBagConstraints);

        JButton connectButton = new JButton("Подключиться");
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        connectButton.addActionListener(this.parent);
        connectButton.setName("Connect");
        this.add(connectButton, gridBagConstraints);
    }
}
