import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;

public class Postman implements SerialPortEventListener, HasLetterToSendListener {
    private ArrayList<LetterReceivedListener> listeners = new ArrayList<>();

    private SerialPort inSerialPort;
    private SerialPort outSerialPort;

    public Postman(String inPortName, String outPortName) {
        try {
            CommPortIdentifier inPortIdentifier = CommPortIdentifier.getPortIdentifier(inPortName);
            if (inPortIdentifier.isCurrentlyOwned()) {
                throw new IllegalArgumentException("Port is currently owned");
            } else {
                CommPort inCommPort = inPortIdentifier.open(this.getClass().getName(), 2000); //TODO: Params?
                if (inCommPort instanceof SerialPort) {
                    inSerialPort = (SerialPort) inCommPort;
                    inSerialPort.setSerialPortParams(115200,
                            SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                    inSerialPort.addEventListener(this);
                    inSerialPort.notifyOnDataAvailable(true);
                } else {
                    throw new IllegalArgumentException("It is not serial port");
                }
            }

            CommPortIdentifier outPortIdentifier = CommPortIdentifier.getPortIdentifier(outPortName);
            if (outPortIdentifier.isCurrentlyOwned()) {
                throw new IllegalArgumentException("Port is currently owned");
            } else {
                CommPort outCommPort = outPortIdentifier.open(this.getClass().getName(), 2000);
                if (outCommPort instanceof SerialPort) {
                    outSerialPort = (SerialPort) outCommPort;
                    outSerialPort.setSerialPortParams(115200,
                            SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                } else {
                    throw new IllegalArgumentException("It is not serial port");
                }
            }
        } catch (Exception e) {
            System.out.println("Postman exception");
        }
    }

    public void serialEvent(SerialPortEvent event) {
        try {
            //Thread.sleep(1000);

            InputStream inputStream = inSerialPort.getInputStream();
            int available = inputStream.available();
            byte[] buffer = new byte[available];
            inputStream.read(buffer);

            for (LetterReceivedListener listener : listeners) {
                listener.recvLetter(buffer);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Input Exception");
        }
    }

    public void sendLetter(byte[] bytes) {
        try {
            outSerialPort.getOutputStream().write(bytes);
        } catch (IOException e) {
            throw new IllegalArgumentException("Output Exception");
        }
    }

    public void addRecvListener(LetterReceivedListener listener) {
        listeners.add(listener);
    }

    public static ArrayList<CommPortIdentifier> getAvailableSerialPorts() {
        ArrayList<CommPortIdentifier> h = new ArrayList<>();
        Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
        while (thePorts.hasMoreElements()) {
            CommPortIdentifier com = (CommPortIdentifier) thePorts.nextElement();
            switch (com.getPortType()) {
                case CommPortIdentifier.PORT_SERIAL:
                    try {
                        CommPort thePort = com.open("CommUtil", 50);
                        thePort.close();
                        h.add(com);
                    } catch (PortInUseException e) {
                        System.out.println("Port, " + com.getName() + ", is in use.");
                    } catch (Exception e) {
                        System.err.println("Failed to open port " + com.getName());
                        e.printStackTrace();
                    }
            }
        }
        return h;
    }
}
