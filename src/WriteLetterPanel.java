import javax.swing.*;
import java.awt.*;

public class WriteLetterPanel extends JPanel {
    private App parent;

    private JTextField addressTextField;
    private JTextField titleTextField;
    private JTextPane letterTextPane;

    public WriteLetterPanel(App parent) {
        super(new GridBagLayout());
        this.parent = parent;
        createUiComponents();
    }

    public boolean isReady() {
        // TODO: Make a validation of the fields.
        return true;
    }

    public Letter getLetter() {
        return new Letter(titleTextField.getText(), letterTextPane.getText(), addressTextField.getText());
    }

    public void setLetter(Letter letter) {
        addressTextField.setText(letter.sender.address);
        titleTextField.setText("Re: " + letter.title);
    }

    private void createUiComponents() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;

        JLabel addressLabel = new JLabel("Адрес получателя");
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        this.add(addressLabel, gridBagConstraints);

        addressTextField = new JTextField();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 2;
        gridBagConstraints.gridwidth = 2;
        this.add(addressTextField, gridBagConstraints);

        JLabel titleLabel = new JLabel("Заголовок");
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.gridwidth = 1;
        this.add(titleLabel, gridBagConstraints);

        titleTextField = new JTextField();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 2;
        gridBagConstraints.gridwidth = 2;
        this.add(titleTextField, gridBagConstraints);

        letterTextPane = new JTextPane();
        letterTextPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY));
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.weightx = 3;
        gridBagConstraints.weighty = 1;
        this.add(letterTextPane, gridBagConstraints);

        JButton backButton = new JButton("На главную");
        backButton.setName("WHome");
        backButton.addActionListener(this.parent);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.weighty = 0;
        this.add(backButton, gridBagConstraints);

        JButton sendButton = new JButton("Отправить");
        sendButton.setName("Send");
        sendButton.addActionListener(this.parent);
        gridBagConstraints.gridx = 2;
        this.add(sendButton, gridBagConstraints);
    }
}
